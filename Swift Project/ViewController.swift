//
//  ViewController.swift
//  Swift Project
//
//  Created by Derek Lurette on 2017-11-02.
//  Copyright © 2017 Derek Lurette. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var theLabel: UILabel!
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    
    @IBAction func buttonPushed(_ sender: Any) {
        
        let addition:Bool = false
        
        if addition {
            theLabel.text = "\(Double(textField1.text!)! + Double(textField2.text!)!)"
        } else {
            theLabel.text = "\(Double(textField1.text!)! - Double(textField2.text!)!)"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

